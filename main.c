#include <X11/Xlib.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char** argv) {

  argc++;

  Display* display;

  /* Open Display */
  display = XOpenDisplay(argv[1]);
  if (display == NULL) {
    fprintf(stderr, "Cannot connect to display");
    return -1;
  }

  int screen_num;
  screen_num = DefaultScreen(display);

  Window root_window;
  root_window = RootWindow(display, screen_num);

  Window window_manager;
  int win_width, win_height, win_x, win_y;
  win_width = DisplayWidth(display, screen_num);
  win_height = DisplayHeight(display, screen_num);
  win_x = win_y = 0;

  window_manager = XCreateSimpleWindow(display, root_window,
                                       win_x, win_y,
                                       win_width,
                                       win_height,
                                       0,
                                       BlackPixel(display, screen_num),
                                       BlackPixel(display, screen_num));

  XSelectInput(display, window_manager, ExposureMask | KeyPressMask | StructureNotifyMask);
  XMapWindow(display, window_manager);

  XEvent event;

  while (1) {
    XNextEvent(display, &event);
    if (event.type == CreateNotify) {
      /* Reparenting new window to WindowManager */
      Window new_window = event.xcreatewindow.window;
      XReparentWindow(display, new_window, window_manager, 0, 0);
    }
  }

  XCloseDisplay(display);
  return 0;
}
