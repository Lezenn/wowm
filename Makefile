CC=gcc
CFLAGS=-Wall -Werror -Wextra
LDFLAGS=-lX11

wowm: main.o
	$(CC) $(CFLAGS) -o $@ $(LDFLAGS) $^

main.o: main.c
	$(CC) -c $(CFLAGS) -o $@ $(LDFLAGS) $^
